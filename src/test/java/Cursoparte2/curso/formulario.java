package Cursoparte2.curso;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class formulario {
	
	private WebDriver driver;
	
	@Before
	public void before() throws Exception 
	{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\rsalvadc\\eclipse-workspace\\cursoAutomatizacionSelenium\\driver\\chromedriver.exe");
				
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		String url = "https://wprsdesaft.profuturo-gnp.net:9450/ProcessServer/ProcessPortal/dashboards/TWP/BPM_WORK?tw.local.view=tasks&tw.local.state=open";
		
		driver.get(url);	
	}
	
	@Test
	public void Ingresar_bpm() throws Exception 
	{
		System.out.println("INGRESAR CREDENCIALES");
		new TestBPM(driver).accion_llenar_login();
		
	}
	
	@After
	public void after() throws InterruptedException
	{
		Thread.sleep(3000);
		driver.quit();
	}
	
	
	
}
