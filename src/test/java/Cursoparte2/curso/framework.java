package Cursoparte2.curso;

import static org.junit.Assert.*;

import java.io.File;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import javax.imageio.ImageIO;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;



public class framework {

	WebDriver driver=null;
	
	
	public framework (WebDriver driver)
	{
		super();
		this.driver=driver;
		
	}
	
	public String getTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy"+" "+"hh_mm_ss");
		Date date= new Date();
		return dateFormat.format(date);
	}
	
	public static void crear()
	{
		String imagenes =".\\imagenes";
		File crea_carpeta=new File(imagenes);
		
		if(crea_carpeta.exists()) 
		{
			System.out.println("Ya existe la carpeta");				
		}
		else
		{
			System.out.println("No existe la carpeta");		
			crea_carpeta.mkdirs();		
		}
	}
		
	public void eventClick(String selector, int tiempo) throws InterruptedException 
	{
		int intentos=0;
		int i=0;
	
		crear();
		
		while(true)
		{
		 try
		 {
			 driver.findElement(By.cssSelector(selector)).click();
				
				Thread.sleep(tiempo);
				
				   Screenshot imagen=new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
			        ImageIO.write(imagen.getImage(),"jpg",new File(".\\imagenes\\Imagen_"+getTime()+".png"));
			        
		 }
		 catch(Exception e)
		 {
			 System.out.println("Error");
			 intentos++;
			 if(intentos==3) 
			 {
				 break;
			 }
		 }
		
		}
		
	}
}
