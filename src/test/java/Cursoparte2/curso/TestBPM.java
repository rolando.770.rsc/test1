package Cursoparte2.curso;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestBPM {
	
	 WebDriver driver;
	
	public TestBPM(WebDriver driver) 
	{
		super();
		this.driver=driver;
	}

	
	public void accion_llenar_login() throws InterruptedException {
		
		//Elementos
		WebElement Mostrar= driver.findElement(By.cssSelector("#details-button"));
		WebElement Entrar= driver.findElement(By.cssSelector("#proceed-link"));
		
		//instrucciones
		Mostrar.click();
		//new framework(driver).eventClick("#details-button",500);
		//Thread.sleep(1000);
		
		new framework(driver).eventClick("#proceed-link",1000);
		
		//Elementos
		WebElement usuario= driver.findElement(By.cssSelector("#username"));
		WebElement contrasena= driver.findElement(By.cssSelector("#password"));
		WebElement btnentrar= driver.findElement(By.cssSelector("#log_in"));
		
		//instrucciones
		usuario.sendKeys("VD500005");
		Thread.sleep(1000);
		contrasena.sendKeys("Profutur0");
		Thread.sleep(1000);
		new framework(driver).eventClick("#log_in",1000);
	
		//Elementos
		WebElement Selec_busqueda= driver.findElement(By.cssSelector("#com_ibm_bpm_social_widgets_LaunchableProcessList_0 > ul > li:nth-child(2) > a"));
		
		//instrucciones
		Thread.sleep(10000);
		new framework(driver).eventClick("#com_ibm_bpm_social_widgets_LaunchableProcessList_0 > ul > li:nth-child(2) > a",3000);
		//Selec_busqueda.click();
		Thread.sleep(10000);
		
	
		
		WebElement iframeElement =driver.findElement(By.xpath("//*[@id=\"com_ibm_bpm_social_widgets_task_form_CoachRenderer_0\"]/iframe"));
		driver.switchTo().frame(iframeElement);
		
		//Elementos
		Thread.sleep(2000);
		
		By Proceso= By.xpath("//*[@id=\"dijit_form_FilteringSelect_1\"]");
		By Subproceso= By.xpath("//*[@id=\"dijit_form_FilteringSelect_2\"]");
		By Estatus= By.xpath("//*[@id=\"dijit_form_FilteringSelect_3\"]");
		By Finicial= By.xpath("//*[@id=\"uniqName_1_0\"]");
		By FFinal= By.xpath("//*[@id=\"uniqName_1_2\"]");
		
		WebElement  Buscar= driver.findElement(By.cssSelector("#div_5_1_1 > button"));
		
		
		//instrucciones
		Thread.sleep(12000);
		System.out.println("Ya pasaron 10 segundos");
		this.driver.findElement(Proceso).sendKeys("RETIRO");
		this.driver.findElement(Proceso).sendKeys(Keys.ENTER);
		
		Thread.sleep(12000);
		this.driver.findElement(Subproceso).sendKeys("CARGA DE PLANES PRIVADOS");
		this.driver.findElement(Subproceso).sendKeys(Keys.ENTER);
		
		Thread.sleep(12000);
		this.driver.findElement(Estatus).sendKeys("FINALIZADO");
		this.driver.findElement(Estatus).sendKeys(Keys.ENTER);
		
		Thread.sleep(12000);
		this.driver.findElement(Finicial).sendKeys("01/01/2021");
		this.driver.findElement(Finicial).sendKeys(Keys.ENTER);
		
		Thread.sleep(12000);
		this.driver.findElement(FFinal).sendKeys("06/03/2021");
		this.driver.findElement(FFinal).sendKeys(Keys.ENTER);
		
		
		//Buscar.click();
		new framework(driver).eventClick("#div_5_1_1 > button",2000);
		
		Thread.sleep(3000);
		WebElement  Cerrar= driver.findElement(By.xpath("//*[@id=\"dijit_Dialog_0\"]/div[1]/span[2]"));
		Cerrar.click();
		driver.switchTo().defaultContent();
	}

}